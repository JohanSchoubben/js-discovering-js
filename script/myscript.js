function helloDude() {
	document.getElementById("x1-03").innerHTML = "What's up, dude?"
}
function textToConsole(message) {
	console.log(message)
}
function displayValue() {
	let message = getValue();
	alert(message)
}
function getValue() {
	let message = document.getElementById("element-to-get").innerHTML;
	// message = Math.PI;
	// const COLOR_BLUE = '#0000FF';
	// message = COLOR_BLUE;
	return message;
}
function getNumbers() {
	alert("20 + 32 = " + (20 + 32));
	alert('5 + "7" = ' + (5 + "7"));
	alert('24 + 16 + "kangaroo" = ' + (24 + 16 + " kangaroo"));
	alert("11 / 0 = " + (11 / 0));
}
function calculateProbability() {
	let n = prompt("Enter a number of people:")
	let x = (364 / 365);
	let y = n * (n - 1) / 2;
	return 'The probability of a shared birthday within a group of ' + n + ' people is ' + ((1 - Math.pow(x, y)) * 100).toFixed(2) + ' %';
}
function checkAge() {
	prompt("Enter your age:") >= 18 ? alert("You can drink, but you can't drive afterwards") : alert("You're below the legal drinking age");
}
function useOperator() {
	let answer = 42;
	alert("original value = " + (document.getElementById("answer").innerHTML = answer));
	alert("+ 7 = " + (document.getElementById("answer").innerHTML = answer += 7));
	alert("- 2 = " + (document.getElementById("answer").innerHTML = answer -= 2));
	alert("remainder after division by 5 = " + (document.getElementById("answer").innerHTML = answer %= 5));
	alert("equals 2 = " + (document.getElementById("answer").innerHTML = answer == 2));
	alert('equals "2" = ' + (document.getElementById("answer").innerHTML = answer == "2"));
	alert("exactly equals 2 = " + (document.getElementById("answer").innerHTML = answer === 2));
	alert('exactly equals "2" = ' + (document.getElementById("answer").innerHTML = answer === "2"));
}
function buildSequence() {
	let increment = 0;
	while (increment < 100) {
		++increment;
		if (increment % 2 === 0) {
			console.log(increment);
		}
	}
}
function generateSequence() {
	for (let i = 1; i <= 100; i++) {
		if (i % 3 === 0 && i % 5 === 0) {
			console.log("fizzbuzz");
		} else if (i % 3 === 0) {
			console.log("fizz");
		} else if (i % 5 === 0) {
			console.log("buzz");
		} else {
			console.log(i);
		}
	}
}
function displayBrowserFeedback() {
	var message;
	let answer = prompt("Which browser are you using?");
	switch (answer) {
		case "Edge":
		case "Chrome":
		case "Firefox":
		case "Safari":
		case "Opera":
			message = "We support you!";
			break;
		default:
			message = "We hope the page looks OK";
			break;
	}
	alert(message);
}
function displayGreetingText() {
	let greeting = document.getElementById("greeting");
	let now = new Date();
	let hour = now.getHours();
	if (hour < 6) {
		greeting.innerHTML = "Good night";
	} else if (hour < 12) {
		greeting.innerHTML = "Good morning";
	} else if (hour < 17) {
		greeting.innerHTML = "Good afternoon";
	} else if (hour < 24) {
		greeting.innerHTML = "Good evening";
	}
}
displayGreetingText();

function checkAgeOnScreen() {
	let message = document.getElementById("age-message").innerHTML = "You'll see our answer here ...";
	let answer = prompt("Enter your age:");
	if (answer >= 18) {
		message = document.getElementById("age-message").innerHTML = "You can drink, but you can't drive afterwards";
	} else {
		message = document.getElementById("age-message").innerHTML = "You're below the legal drinking age!";
	}
}

function minVal(a, b) {
	let result = document.getElementById("result").innerHTML = "";
	a = prompt("Enter your first number");
	b = prompt("Enter your second number");
	if (a < b) {
		result = document.getElementById("result").innerHTML = "The smallest number is " + a;
	} else {
		result = document.getElementById("result").innerHTML = "The smallest number is " + b;
	}
}
function askYOrN() {
	let question = "Do you want to proceed?"
	if (confirm(question)) {
		yes();
	} else {
		no();
	}
}
function yes() {
	alert("okay");
}
function no() {
	alert("cancelled");
}

let question = "Are you a good cook?";
let ok = function () {
	alert("I'll invite myself then ... ");
}
let cancel = function () {
	alert("That's disappointing ... I'd better get the pots out!");
}
function ask(question, ok, cancel) {
	if (confirm(question)) {
		ok();
	} else {
		cancel();
	}
}

function askForNames() {
	let firstName = prompt("Enter your first name");
	let lastName = prompt("Enter your last name");
	document.getElementById("full-name").innerHTML = firstName + " " + lastName;
}

let faveNum = 30;
function scope() {
	faveNum = 60;
	function innerScope() {
		return faveNum;
	}
	return innerScope();
}
console.log(faveNum);
console.log(scope());

let primeNumbers = [2, 3, 5, 7];
function displayPrimeNumbers() {
	alert('initial array = ' + primeNumbers);
	primeNumbers.push(11);
	alert('add 11 to array => ' + primeNumbers);
	primeNumbers[4] = "eleven";
	alert('change index 4 to "eleven" => ' + primeNumbers);
	if (primeNumbers.length > 4) {
		primeNumbers.pop();
	}
	alert('remove last element if length > 4 => ' + primeNumbers);
	alert('show index of number 7 => ' + primeNumbers.indexOf(7));
}

function askForNumbers() {
	let numbers = [];
	let input = prompt("Enter a number");
	while (input) {
		numbers.push(input);
	}
	alert(numbers);
}
class Person {
	constructor(fName, lName, gender, dob) {
		this.fName = fName;
		this.lName = lName;
		this.gender = gender;
		this.dob = dob;
	}
	calcAge() {
		let now = new Date();
		let age = now.getFullYear() - this.dob.getFullYear();
		if (now.getMonth() < this.dob.getMonth()) {
			age--;
		} else if (now.getMonth() === this.dob.getMonth()
			&& now.getDay() < this.dob.getDay()) {
			age--;
		}
		return age;
	}
	introduce() {
		return (
			"My name is " +
			this.fName + " " + this.lName +
			" and I am a " + this.calcAge() + " years old " + this.gender
		);
	}
}
function displayPeople() {
	let miet = new Person("Miet", "Lemmens", "female", new Date(1985, 10, 9));
	let people = [miet];
	people.push(new Person("Jan", "Smets", "male", new Date(1980, 9, 16)));
	people.push(new Person("An", "Vandormael", "female", new Date(1970, 2, 29)));
	let peopleDisplay = document.getElementById("people");
	peopleDisplay.innerHTML = "";

	for (let person of people) {
		let p = document.createElement("p");
		p.innerHTML = person.introduce();
		peopleDisplay.append(p);
	}
}

let exercise = document.getElementById("exercise-add-button");
exercise.append(createButton("Automatically added button"));

function createButton(text) {
	let button = document.createElement("button");
	button.classList += "btn btn-primary";
	button.innerHTML = text;
	return button;
}

// let exerc = document.getElementById("exercise-add-function");
// exerc.append(createFuncBtn("Button has a function", yes));

// function createFuncBtn(text, func) {
// 	let button = createButton(text);
// 	button.onclick = func;
// 	return button;
// }

// let ex = document.getElementById("exercise-add-code");
// ex.append(createFuncBtn("Add code to page", no));

// function addCode() {
// let code = document.getElementById("add-code", createButton);


// }
let exerc = document.getElementById("exercise-add-function");
exerc.append(createFuncBtn("Button has a function", "yes()"));

function createFuncBtn(text, functionAsString) {
	let button = createButton(text);
	// button.onclick = func;
	button.setAttribute("onclick", functionAsString);
	return button;
}


let ex = document.getElementById("exercise-add-code");
ex.append(createFuncBtn("Add code to page", 'addCode("add-code", addCode)'));

function addCode(id, otherFunc) {
	let codeElement = document.getElementById(id);
	codeElement.innerHTML = codeElement.innerHTML.replace(/[<]br[/]?[>]/, "\n");
	codeElement.append(otherFunc);
	Prism.highlightElement(codeElement);
}

function askForNumbers() {
	let arrayOfNumbers = new Array();
	let input = function () { prompt("Enter a number") };
	// let checkInput = (input === null || Number.isFinite(input));
	let sum = 0;
	while (arrayOfNumbers.length < 10) {
		input();
		if (input !== null) {
			arrayOfNumbers.push(input);
		} else {
			break;
		}
	}
	alert(arrayOfNumbers);
}
